package com.ppb.openday.livedata.persistence.converters;

public interface Converter<O, I> {
    O convert(I input);
}
